package com.example.project;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.example.project.Views.Designing.FragmentHelper;
import com.example.project.Views.Designing.ViewPagerAdapter;
import com.example.project.Views.Fragment.EachCategoryFragment;
import com.example.project.Views.Fragment.EndlessFragment;
import com.example.project.Views.Fragment.ExploreFragment;
import com.example.project.Views.Fragment.HomeFragment;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private EndlessFragment endlessFragment = new EndlessFragment();
    private ExploreFragment exploreFragment = new ExploreFragment();
    private HomeFragment homeFragment = new HomeFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout=findViewById(R.id.tab_layout);

        FragmentHelper.add(this, R.id.view_pager_container, homeFragment);

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                Fragment fragment = null;
                switch (position) {
                    case 0:
                        fragment = homeFragment;
                        break;
                    case 1:
                        fragment = endlessFragment;
                        break;
                    case 2:
                        fragment = exploreFragment;
                        break;
                }
                if (fragment != null) {
                    FragmentHelper.replace(MainActivity.this,
                            R.id.view_pager_container, fragment);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_layers_black_24dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_explore_black_24dp);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ff7315"));
        tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#ff7315"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tabLayout.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#ffffff"));

    }
    static public void replace(AppCompatActivity compatActivity, Fragment fragment,Bundle bundle) {
        FragmentManager fm = compatActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        fragment.setArguments(bundle);
        ft.replace(R.id.view_pager_container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}
