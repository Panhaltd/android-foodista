package com.example.project.ViewModel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.Model.api.repository.ArticleRepository;
import com.example.project.Model.api.response.ListFoodResponse;

public class ListArticleViewModel extends ViewModel {

    private MutableLiveData<ListFoodResponse> categoryData,liveData,bestData,breakfastData,lunchData,dinnerData,drinkData,fastData,vegetarianData,dessertData,healthyData;
    private ArticleRepository articleRepository;
    private ListFoodResponse listFoodResponse;
    private MutableLiveData<ListFoodResponse> triggerData = new MutableLiveData<>();

    public ListFoodResponse getListFoodResponse(){
        return listFoodResponse;
    }

    public void init(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse();
        liveData=articleRepository.getListArticleByListFoodResponse(listFoodResponse);
    }

    public void fetchListArticleByPaging() {
        triggerData.setValue(articleRepository.getListArticleByListFoodResponse(listFoodResponse).getValue());
    }

    public void categoryInit(String category){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(20,1);
        categoryData=articleRepository.getCategoryByListFoodResponse(listFoodResponse,category);
    }

    public void bestInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        bestData=articleRepository.getBestByListFoodResponse(listFoodResponse);
    }

    public void breakfastInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        breakfastData=articleRepository.getBreakfastByListFoodResponse(listFoodResponse);
    }

    public void lunchInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        lunchData=articleRepository.getLunchByListFoodResponse(listFoodResponse);
    }

    public void dinnerInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        dinnerData=articleRepository.getDinnerByListFoodResponse(listFoodResponse);
    }

    public void drinkInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        drinkData=articleRepository.getDrinkByListFoodResponse(listFoodResponse);
    }

    public void fastfoodInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        fastData=articleRepository.getFastFoodByListFoodResponse(listFoodResponse);
    }

    public void healthyInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        healthyData=articleRepository.getHealthyByListFoodResponse(listFoodResponse);
    }

    public void vegetarianInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        vegetarianData=articleRepository.getVegetarianByListFoodResponse(listFoodResponse);
    }

    public void dessertInit(){
        articleRepository = new ArticleRepository();
        listFoodResponse=new ListFoodResponse(5,1);
        dessertData=articleRepository.getDessertByListFoodResponse(listFoodResponse);
    }

    public MutableLiveData<ListFoodResponse> getBestData(){
        return bestData;
    }
    public MutableLiveData<ListFoodResponse> getBreakfastData(){
        return breakfastData;
    }
    public MutableLiveData<ListFoodResponse> getLunchData(){
        return lunchData;
    }
    public MutableLiveData<ListFoodResponse> getDinnerData(){
        return dinnerData;
    }
    public MutableLiveData<ListFoodResponse> getDrinkData(){
        return drinkData;
    }
    public MutableLiveData<ListFoodResponse> getFastData(){
        return fastData;
    }
    public MutableLiveData<ListFoodResponse> getVegetarianData(){
        return vegetarianData;
    }
    public MutableLiveData<ListFoodResponse> getHealthyData(){
        return healthyData;
    }
    public MutableLiveData<ListFoodResponse> getCategoryData(){
        return categoryData;
    }
    public MutableLiveData<ListFoodResponse> getDessertData(){
        return dessertData;
    }
    public MutableLiveData<ListFoodResponse> getLiveData(){
        return liveData;
    }

}
