package com.example.project.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.Model.api.repository.IngredientRepository;
import com.example.project.Model.api.response.IngredientResponse;

public class IngredientViewModel extends ViewModel {

    private MutableLiveData<IngredientResponse> ingredientData;
    private IngredientRepository ingredientRepository;
    private IngredientResponse ingredientResponse;
    private String id;

    public void ingredientInit(String id){
        ingredientRepository=new IngredientRepository();
        ingredientResponse=new IngredientResponse();
        ingredientData=ingredientRepository.getIngredientByResponse(ingredientResponse,id);
    }
    public MutableLiveData<IngredientResponse> getIngredientData(){
        return ingredientData;
    }
}
