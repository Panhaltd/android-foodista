package com.example.project.Views.Fragment;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.project.MainActivity;
import com.example.project.Model.api.response.ListFoodResponse;
import com.example.project.R;
import com.example.project.ViewModel.ListArticleViewModel;
import com.example.project.Views.Designing.BestFoodHomeRecyclerAdapter;
import com.example.project.Views.Designing.EndlessAdapter;
import com.example.project.Views.Designing.SearchRecyclerAdapter;
import com.example.project.Views.Designing.TopFoodOnRecyclerClick;
import com.example.project.Views.Designing.TopFoodRecyclerAdapter;
import com.example.project.Views.SimpleEntity.TopFoodEntity;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.ArrayList;

public class ExploreFragment extends Fragment implements TopFoodOnRecyclerClick {

    private EditText etSearch;
    private RelativeLayout containerExplore;
    private RecyclerView rcvExplore;
    private EndlessAdapter adapter;
    private ListFoodResponse dataSet;
    private TextView tvBreakFast,tvLunch,tvDinner,tvDrink,tvFastFood,tvHealthy,tvVegetarian,tvDessert;
    private ListArticleViewModel listArticleViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewFragment = inflater.inflate(R.layout.explore_fragment_layout, null);


        etSearch=viewFragment.findViewById(R.id.etSearch);
        containerExplore=viewFragment.findViewById(R.id.explore_container);
        initTextView(viewFragment);


        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();

        etSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)&etSearch.getText().toString().length()<1) {
                        containerExplore.removeAllViewsInLayout();
                        addCategoryBack();
                        UIUtil.hideKeyboard((Activity) getContext());
                        return true;
                    }else if((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)&etSearch.getText().toString().length()>0){
                            liveViewModel(etSearch.getText().toString());
                            UIUtil.hideKeyboard((Activity) getContext());
                            return true;
                }
                return false;
            }
        });

        return viewFragment;
    }
    private ListFoodResponse getLiveDataFromViewModel(){
        listArticleViewModel.getCategoryData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet=listFoodResponse;
                if (dataSet.getResults().size()==0){
                    notFound();
                }else {
                    rcvExplore=new RecyclerView(getContext());
                    adapter=new EndlessAdapter(dataSet,ExploreFragment.this::onItemClick,getContext());
                    LinearLayoutManager layoutManager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
                    rcvExplore.setLayoutManager(layoutManager);
                    rcvExplore.setAdapter(adapter);
                    //create layout param for recycler
                    RelativeLayout.LayoutParams rcvParam =  new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

                    rcvExplore.setLayoutParams(rcvParam);
                    containerExplore.addView(rcvExplore);
                    adapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                        @Override
                        public void onItemClick(int position) {
                            String id = dataSet.getResults().get(position).getId()+ "";
                            Bundle bundle = new Bundle();
                            bundle.putString("id",id);
                            MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                        }
                    });
                }
            }
        });
        return dataSet;
    }
    public void setUpRCV(){

    }
    private void initTextView(View view){
        tvBreakFast=view.findViewById(R.id.breakfast_ex);
        tvLunch=view.findViewById(R.id.lunch_ex);
        tvDinner=view.findViewById(R.id.dinner_ex);
        tvDrink=view.findViewById(R.id.drink_ex);
        tvFastFood=view.findViewById(R.id.fastfood_ex);
        tvHealthy=view.findViewById(R.id.healthy_ex);
        tvVegetarian=view.findViewById(R.id.vegetarian_ex);
        tvDessert=view.findViewById(R.id.dessert_ex);
        tvBreakFast.setOnClickListener(v -> {
            liveViewModel("breakfast");
        });
        tvLunch.setOnClickListener(v -> {
            liveViewModel("lunch");
        });
        tvDinner.setOnClickListener(v -> {
            liveViewModel("soup");
        });
        tvDrink.setOnClickListener(v -> {
            liveViewModel("drink");
        });
        tvFastFood.setOnClickListener(v -> {
            liveViewModel("burger");
        });
        tvHealthy.setOnClickListener(v -> {
            liveViewModel("healthy");
        });
        tvVegetarian.setOnClickListener(v -> {
            liveViewModel("vegetarian");
        });
        tvDessert.setOnClickListener(v -> {
            liveViewModel("dessert");
        });
    }

    @Override
    public void onItemClick ( int position){

    }

    public void liveViewModel(String category){
        containerExplore.removeAllViewsInLayout();
        listArticleViewModel= ViewModelProviders.of(getActivity()).get(ListArticleViewModel.class);
        listArticleViewModel.categoryInit(category);
        getLiveDataFromViewModel();
    }

    public void notFound(){
        TextView notFound = new TextView(getContext());
        notFound.setText("Search Not Found!!!");
        notFound.setTypeface(notFound.getTypeface(), Typeface.BOLD);
        notFound.setTextSize(22);
        notFound.setGravity(Gravity.CENTER);
        RelativeLayout.LayoutParams layoutNotFound = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutNotFound.setMargins(0,30,0,0);
        notFound.setLayoutParams(layoutNotFound);
        containerExplore.addView(notFound);
    }


    public void addCategoryBack(){
        TextView breakfast_ex = null,lunch_ex=null,dinner_ex=null,drink_ex=null,fastfood_ex=null,healthy_ex=null,vegetarian_ex=null,dessert_ex=null;
        setUpFirstText("Breakfast Menu!",breakfast_ex,R.id.breakfast_ex,R.drawable.breakfast_back);
        setUpTextNext("Lunch Menu!",lunch_ex,R.id.breakfast_ex,R.id.lunch_ex,R.drawable.lunch_back);
        setUpTextNext("Dinner Menu!",dinner_ex,R.id.lunch_ex,R.id.dinner_ex,R.drawable.dinner_back);
        setUpTextNext("Drink Menu!",drink_ex,R.id.dinner_ex,R.id.drink_ex,R.drawable.drink_back);
        setUpTextNext("Fastfood Menu!",fastfood_ex,R.id.drink_ex,R.id.fastfood_ex,R.drawable.fastfood_back);
        setUpTextNext("Healthy Menu!",healthy_ex,R.id.fastfood_ex,R.id.healthy_ex,R.drawable.healthy_back);
        setUpTextNext("Vegetarian Menu!",vegetarian_ex,R.id.healthy_ex,R.id.vegetarian_ex,R.drawable.vegetarian_back);
        setUpTextNext("Dessert Menu!",dessert_ex,R.id.vegetarian_ex,R.id.dessert_ex,R.drawable.dessert_back);
    }
    public void setUpTextNext(String category,TextView categoryID,int id1,int id2,int resId){
        categoryID = new TextView(getContext());
        categoryID.setId(id2);
        categoryID.setText(category);
        categoryID.setPadding(10,18,10,18);
        categoryID.setTextColor(Color.parseColor("#000000"));
        categoryID.setTypeface(categoryID.getTypeface(), Typeface.BOLD);
        categoryID.setBackgroundResource(resId);
        categoryID.setTextSize(22);
        RelativeLayout.LayoutParams layout_564 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        layout_564.setMargins(0,30,0,0);
        layout_564.addRule(RelativeLayout.BELOW,id1);
        categoryID.setLayoutParams(layout_564);
        containerExplore.addView(categoryID);
        categoryID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int resId=v.getId();
                switch (resId){
                    case R.id.lunch_ex:
                        liveViewModel("lunch");
                        break;
                    case R.id.dinner_ex:
                        liveViewModel("soup");
                        break;
                    case R.id.drink_ex:
                        liveViewModel("drink");
                        break;
                    case R.id.fastfood_ex:
                        liveViewModel("burger");
                        break;
                    case R.id.healthy_ex:
                        liveViewModel("healthy");
                        break;
                    case R.id.vegetarian_ex:
                        liveViewModel("vegetarian");
                        break;
                    case R.id.dessert_ex:
                        liveViewModel("dessert");
                        break;

                }
            }
        });

    }
    public void setUpFirstText(String category,TextView categoryID,int id,int resId){
        categoryID = new TextView(getContext());
        categoryID.setId(id);
        categoryID.setText(category);
        categoryID.setPadding(10,18,10,18);
        categoryID.setTextColor(Color.parseColor("#000000"));
        categoryID.setTypeface(categoryID.getTypeface(), Typeface.BOLD);
        categoryID.setBackgroundResource(resId);
        categoryID.setTextSize(22);
        RelativeLayout.LayoutParams layout_564 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        layout_564.setMargins(0,30,0,0);
        categoryID.setLayoutParams(layout_564);
        containerExplore.addView(categoryID);
        categoryID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liveViewModel("breakfast");
            }
        });
    }



    
    
    }


