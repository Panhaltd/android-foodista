package com.example.project.Views.Designing;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.Views.SimpleEntity.TopFoodEntity;

import java.util.ArrayList;

public class TopFoodRecyclerAdapter extends RecyclerView.Adapter<TopFoodRecyclerAdapter.TopFoodViewHolder> {
    private ArrayList<TopFoodEntity> topFoodList = new ArrayList<>();
    private TopFoodOnRecyclerClick listener;

    public TopFoodRecyclerAdapter(ArrayList<TopFoodEntity> topFoodList, TopFoodOnRecyclerClick listener) {
        this.topFoodList = topFoodList;
        this.listener = listener;
    }


    @NonNull
    @Override
    public TopFoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_list_item,parent,false);
        return new TopFoodViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TopFoodViewHolder holder, int position) {
        holder.topSlideImage.setImageResource(topFoodList.get(position).getImageTopFood());
        holder.topSlideTitle.setText(topFoodList.get(position).getTitleTopFood());
    }

    @Override
    public int getItemCount() {
        return topFoodList.size();
    }

    public class TopFoodViewHolder extends RecyclerView.ViewHolder {
        ImageView topSlideImage;
        TextView topSlideTitle;
        public TopFoodViewHolder(@NonNull View itemView) {
            super(itemView);


        }
    }

}
