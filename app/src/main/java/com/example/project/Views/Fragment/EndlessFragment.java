package com.example.project.Views.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.project.MainActivity;
import com.example.project.Model.api.response.ListFoodResponse;
import com.example.project.R;
import com.example.project.ViewModel.ListArticleViewModel;
import com.example.project.Views.Designing.EndlessAdapter;
import com.example.project.Views.Designing.TopFoodOnRecyclerClick;

public class EndlessFragment extends Fragment implements TopFoodOnRecyclerClick {

    private RecyclerView rcvEndLessView;
    private ListFoodResponse dataSet;
    private EndlessAdapter endLessAdapter;
    private LinearLayoutManager layoutManager;
    private ListArticleViewModel listArticleViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewFragment = inflater.inflate(R.layout.endless_fragment_layout, null);
        rcvEndLessView=viewFragment.findViewById(R.id.endLess_scrollView);

        listArticleViewModel= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        listArticleViewModel.init();
        getLiveDataFromViewModel();
        fetchListArticle();
        return viewFragment;
    }

    void setUpRCV(){
            endLessAdapter=new EndlessAdapter(dataSet,this,getContext());
            layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
            rcvEndLessView.setLayoutManager(layoutManager);
            rcvEndLessView.setAdapter(endLessAdapter);
    }

    private void fetchListArticle() {
        listArticleViewModel.fetchListArticleByPaging();
    }
    private void getLiveDataFromViewModel(){
        listArticleViewModel.getLiveData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet=listFoodResponse;
                setUpRCV();
                endLessAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });
            }
        });
    }

    @Override
    public void onItemClick(int position) {

    }

}
