package com.example.project.Views.Designing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.project.Model.api.response.ListFoodResponse;
import com.example.project.R;
import com.example.project.Views.SimpleEntity.TopFoodEntity;

import java.util.ArrayList;

public class SearchRecyclerAdapter extends RecyclerView.Adapter<SearchRecyclerAdapter.SearchViewHolder> {

    private Context context;
    private ListFoodResponse dataSet;

    public SearchRecyclerAdapter(Context context,ListFoodResponse dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    @NonNull
    @Override
    public SearchRecyclerAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_list_item,parent,false);
        return new SearchViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchRecyclerAdapter.SearchViewHolder holder, int position) {
        Glide.with(context).load(dataSet.getResults().get(position).getImage()).into(holder.searchImage);
        holder.searchTitle.setText(dataSet.getResults().get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {

        ImageView searchImage;
        TextView searchTitle;

        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);
            searchImage=itemView.findViewById(R.id.search_image);
            searchTitle=itemView.findViewById(R.id.search_title);
        }
    }
}
