package com.example.project.Views.SimpleEntity;

public class TopFoodEntity {

    private Integer imageTopFood;
    private String titleTopFood;

    public TopFoodEntity(Integer imageTopFood,String titleTopFood) {
        this.imageTopFood=imageTopFood;
        this.titleTopFood = titleTopFood;
    }

    public Integer getImageTopFood() {
        return imageTopFood;
    }

    public void setImageTopFood(Integer imageTopFood) {
        this.imageTopFood = imageTopFood;
    }

    public String getTitleTopFood() {
        return titleTopFood;
    }

    public void setTitleTopFood(String titleTopFood) {
        this.titleTopFood = titleTopFood;
    }

}
