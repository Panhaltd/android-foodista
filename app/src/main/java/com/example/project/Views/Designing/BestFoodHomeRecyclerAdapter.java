package com.example.project.Views.Designing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.project.Model.api.response.ListFoodResponse;
import com.example.project.R;
import com.example.project.Views.SimpleEntity.BestFoodEntity;

import java.util.ArrayList;

import static com.example.project.R.id.pic_ready_home;
import static com.example.project.R.id.pic_serve_home;

public class BestFoodHomeRecyclerAdapter extends RecyclerView.Adapter<BestFoodHomeRecyclerAdapter.BestFoodViewHolder>{

    private Context context;
    private TopFoodOnRecyclerClick listener;
    private ListFoodResponse dataSet;

    public BestFoodHomeRecyclerAdapter(ListFoodResponse dataSet, TopFoodOnRecyclerClick listener,Context context) {
        this.dataSet = dataSet;
        this.listener = listener;
        this.context=context;
    }

    @NonNull
    @Override
    public BestFoodHomeRecyclerAdapter.BestFoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_page_view,parent,false);
        return new BestFoodViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BestFoodHomeRecyclerAdapter.BestFoodViewHolder holder, int position) {
        Glide.with(context).load(dataSet.getBaseUri()+
                dataSet.getResults().get(position).getImage()).into(holder.ImageBestFood);
        holder.TitleBestFood.setText(dataSet.getResults().get(position).getTitle());
        holder.MinuteBestFood.setText(dataSet.getResults().get(position).getReadyInMinutes()+" minutes");
        holder.ServingBestFood.setText("For "+dataSet.getResults().get(position).getServings()+" people");
    }

    public void setOnItemClickListener(TopFoodOnRecyclerClick listener)
    {
        this.listener=listener;
    }


    @Override
    public int getItemCount() {
        return dataSet.getResults().size();
    }

    public class BestFoodViewHolder extends RecyclerView.ViewHolder {
        ImageView ImageBestFood;
        TextView TitleBestFood,ServingBestFood,MinuteBestFood;

        public BestFoodViewHolder(@NonNull View itemView) {
            super(itemView);
            ImageBestFood=itemView.findViewById(R.id.bestFoodImage);
            TitleBestFood=itemView.findViewById(R.id.pic_title_home);
            ServingBestFood=itemView.findViewById(pic_serve_home);
            MinuteBestFood=itemView.findViewById(pic_ready_home);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if (position!=RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            });

        }
    }
}
