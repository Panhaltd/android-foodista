package com.example.project.Views.Designing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.project.Model.api.response.ListFoodResponse;
import com.example.project.Model.entity.ResultsEntity;
import com.example.project.R;
import com.example.project.Views.Fragment.EndlessFragment;

import java.util.ArrayList;
import java.util.List;

import static com.example.project.R.id.endless_image;

public class EndlessAdapter extends RecyclerView.Adapter<EndlessAdapter.EndLessViewHolder>{

    private ListFoodResponse dataSet;
    private TopFoodOnRecyclerClick listener;
    private Context context;

    public EndlessAdapter(ListFoodResponse dataSet, TopFoodOnRecyclerClick listener,Context context) {
        this.dataSet = dataSet;
        this.listener = listener;
        this.context=context;
    }

    public void setDataSet(ListFoodResponse dataSet){
        this.dataSet=dataSet;
    }
    public void appendDataSet(ListFoodResponse dataSet) {
        this.dataSet=dataSet;
    }
    @NonNull
    @Override
    public EndlessAdapter.EndLessViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.endless_item,parent,false);
        return new EndLessViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull EndlessAdapter.EndLessViewHolder holder, int position) {
        Glide.with(context).load(dataSet.getBaseUri()+
                dataSet.getResults().get(position).getImage()).into(holder.ImageEndless);
//        holder.ImageEndless.setImageResource(R.drawable.food1);
        holder.TitleEndless.setText(dataSet.getResults().get(position).getTitle());
        holder.MinuteEndless.setText("Ready in "+dataSet.getResults().get(position).getReadyInMinutes()+" minutes");
        holder.ServingEndless.setText(dataSet.getResults().get(position).getServings()+" people serving");
    }

    @Override
    public int getItemCount() {
        return dataSet.getResults().size();
    }

    public void setOnItemClickListener(TopFoodOnRecyclerClick listener)
    {
        this.listener=listener;
    }



    public class EndLessViewHolder extends RecyclerView.ViewHolder {

        ImageView ImageEndless;
        TextView TitleEndless,ServingEndless,MinuteEndless;

        public EndLessViewHolder(@NonNull View itemView,TopFoodOnRecyclerClick listener) {
            super(itemView);
            ImageEndless=itemView.findViewById(endless_image);
            TitleEndless=itemView.findViewById(R.id.endless_title);
            ServingEndless=itemView.findViewById(R.id.endless_serving);
            MinuteEndless=itemView.findViewById(R.id.endless_minute);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if (position!=RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            });
        }
    }

}
