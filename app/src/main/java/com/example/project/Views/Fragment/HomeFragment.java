package com.example.project.Views.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.project.MainActivity;
import com.example.project.Model.api.response.ListFoodResponse;
import com.example.project.R;
import com.example.project.ViewModel.ListArticleViewModel;
import com.example.project.Views.Designing.BestFoodHomeRecyclerAdapter;
import com.example.project.Views.Designing.EndlessAdapter;
import com.example.project.Views.Designing.FragmentHelper;
import com.example.project.Views.Designing.TopFoodOnRecyclerClick;
import com.example.project.Views.SimpleEntity.BestFoodEntity;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements TopFoodOnRecyclerClick{

    private RecyclerView recyclerBestFood,rcvBreakFast,rcvLunch,rcvDinner,rcvDrink,rcvFastFood,rcvVegetarian,rcvHealthy,rcvDessert;
    private BestFoodHomeRecyclerAdapter bestFoodAdapter,breakFastAdapter,lunchAdapter,dinnerAdapter,healthyAdapter,drinkAdapter,fastFoodAdapter,vegetarianAdapter,dessertAdapter;
    private FrameLayout breakFastFrame,lunchFrame,dinnerFrame,drinkFrame,vegetarianFrame,healthyFrame,fastFoodFrame,dessertFrame;
    private ListArticleViewModel bestFoodVM,breakFastVM,lunchVM,dinnerVM,drinkVM,healthyVM,fastFoodVM,vegetarianVM,dessertVM;
    private ListFoodResponse dataSet1,dataSet2,dataSet3,dataSet4,dataSet5,dataSet6,dataSet7,dataSet8,dataSet9;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewFragment = inflater.inflate(R.layout.home_fragment_layout,null);

        initAllRecycler(viewFragment);
        initAllFrame(viewFragment);

        return viewFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAllLayoutManager();

        initAllSnapHelper();

        initAllViewModel();

        getLiveDataFromViewModel();

        breakFastFrame.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("category","breakfast");
            MainActivity.replace((AppCompatActivity) getActivity(),new EachCategoryFragment(),bundle);
        });
        lunchFrame.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("category","lunch");
            MainActivity.replace((AppCompatActivity) getActivity(),new EachCategoryFragment(),bundle);
        });
        dinnerFrame.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("category","soup");
            MainActivity.replace((AppCompatActivity) getActivity(),new EachCategoryFragment(),bundle);
        });
        drinkFrame.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("category","drink");
            MainActivity.replace((AppCompatActivity) getActivity(),new EachCategoryFragment(),bundle);
        });
        fastFoodFrame.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("category","burger");
            MainActivity.replace((AppCompatActivity) getActivity(),new EachCategoryFragment(),bundle);
        });
        healthyFrame.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("category","healthy");
            MainActivity.replace((AppCompatActivity) getActivity(),new EachCategoryFragment(),bundle);
        });
        vegetarianFrame.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("category","vegetarian");
            MainActivity.replace((AppCompatActivity) getActivity(),new EachCategoryFragment(),bundle);
        });
        dessertFrame.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("category","dessert");
            MainActivity.replace((AppCompatActivity) getActivity(),new EachCategoryFragment(),bundle);
        });
    }

    @Override
    public void onItemClick(int position) {

    }

    public void initAllFrame(View viewFragment){
        breakFastFrame=viewFragment.findViewById(R.id.breakfast_frame);
        lunchFrame=viewFragment.findViewById(R.id.lunch_frame);
        dinnerFrame=viewFragment.findViewById(R.id.dinner_frame);
        drinkFrame=viewFragment.findViewById(R.id.drink_frame);
        fastFoodFrame=viewFragment.findViewById(R.id.fastfood_frame);
        healthyFrame=viewFragment.findViewById(R.id.healthy_frame);
        vegetarianFrame=viewFragment.findViewById(R.id.vegetarian_frame);
        dessertFrame=viewFragment.findViewById(R.id.dessert_frame);
    }

    public void initAllViewModel(){
        bestFoodVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        bestFoodVM.bestInit();
        breakFastVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        breakFastVM.breakfastInit();
        lunchVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        lunchVM.lunchInit();
        dinnerVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        dinnerVM.dinnerInit();
        healthyVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        healthyVM.healthyInit();
        fastFoodVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        fastFoodVM.fastfoodInit();
        drinkVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        drinkVM.drinkInit();
        vegetarianVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        vegetarianVM.vegetarianInit();
        dessertVM= ViewModelProviders.of(this).get(ListArticleViewModel.class);
        dessertVM.dessertInit();
    }

    public void initAllRecycler(View viewFragment){
        recyclerBestFood=viewFragment.findViewById(R.id.BestFoodHomePage);
        rcvBreakFast=viewFragment.findViewById(R.id.breakfast_top_5);
        rcvLunch=viewFragment.findViewById(R.id.lunch_top_5);
        rcvDinner=viewFragment.findViewById(R.id.dinner_top_5);
        rcvHealthy=viewFragment.findViewById(R.id.healthy_top_5);
        rcvDrink=viewFragment.findViewById(R.id.drink_top_5);
        rcvFastFood=viewFragment.findViewById(R.id.fastfood_top_5);
        rcvVegetarian=viewFragment.findViewById(R.id.vegetarian_top_5);
        rcvDessert=viewFragment.findViewById(R.id.dessert_top_5);
    }
    public void initAllLayoutManager(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager layoutManagerBreakfast = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager layoutManagerLunch = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager layoutManagerDinner = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager layoutManagerHealthy = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager layoutManagerDrink = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager layoutManagerVegetarian = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager layoutManagerFastFood = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        LinearLayoutManager layoutManagerDessert = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);

        recyclerBestFood.setLayoutManager(layoutManager);
        rcvBreakFast.setLayoutManager(layoutManagerBreakfast);
        rcvLunch.setLayoutManager(layoutManagerLunch);
        rcvDinner.setLayoutManager(layoutManagerDinner);
        rcvHealthy.setLayoutManager(layoutManagerHealthy);
        rcvDrink.setLayoutManager(layoutManagerDrink);
        rcvFastFood.setLayoutManager(layoutManagerFastFood);
        rcvVegetarian.setLayoutManager(layoutManagerVegetarian);
        rcvDessert.setLayoutManager(layoutManagerDessert);
    }

    public void initAllSnapHelper(){
        SnapHelper snapHelper = new PagerSnapHelper();
        SnapHelper snapHelper1 = new PagerSnapHelper();
        SnapHelper snapHelper2 = new PagerSnapHelper();
        SnapHelper snapHelper3 = new PagerSnapHelper();
        SnapHelper snapHelper4 = new PagerSnapHelper();
        SnapHelper snapHelper5 = new PagerSnapHelper();
        SnapHelper snapHelper6 = new PagerSnapHelper();
        SnapHelper snapHelper7 = new PagerSnapHelper();
        SnapHelper snapHelper8 = new PagerSnapHelper();

        snapHelper.attachToRecyclerView(recyclerBestFood);
        snapHelper2.attachToRecyclerView(rcvBreakFast);
        snapHelper1.attachToRecyclerView(rcvDinner);
        snapHelper3.attachToRecyclerView(rcvLunch);
        snapHelper4.attachToRecyclerView(rcvDrink);
        snapHelper5.attachToRecyclerView(rcvHealthy);
        snapHelper6.attachToRecyclerView(rcvFastFood);
        snapHelper7.attachToRecyclerView(rcvVegetarian);
        snapHelper8.attachToRecyclerView(rcvDessert);
    }

    public void getLiveDataFromViewModel(){
        bestFoodVM.getBestData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet1=listFoodResponse;
                bestFoodAdapter=new BestFoodHomeRecyclerAdapter(dataSet1,HomeFragment.this::onItemClick,getContext());
                recyclerBestFood.setAdapter(bestFoodAdapter);
                bestFoodAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet1.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });

            }
        });
        breakFastVM.getBreakfastData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet2=listFoodResponse;
                breakFastAdapter=new BestFoodHomeRecyclerAdapter(dataSet2,HomeFragment.this::onItemClick,getContext());
                rcvBreakFast.setAdapter(breakFastAdapter);
                breakFastAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet2.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });

            }
        });
        lunchVM.getLunchData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet3=listFoodResponse;
                lunchAdapter=new BestFoodHomeRecyclerAdapter(dataSet3,HomeFragment.this::onItemClick,getContext());
                rcvLunch.setAdapter(lunchAdapter);
                lunchAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet3.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });
            }
        });
        dinnerVM.getDinnerData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet4=listFoodResponse;
                dinnerAdapter=new BestFoodHomeRecyclerAdapter(dataSet4,HomeFragment.this::onItemClick,getContext());
                rcvDinner.setAdapter(dinnerAdapter);
                dinnerAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet4.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });
            }
        });
        drinkVM.getDrinkData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet5=listFoodResponse;
                drinkAdapter=new BestFoodHomeRecyclerAdapter(dataSet5,HomeFragment.this::onItemClick,getContext());
                rcvDrink.setAdapter(drinkAdapter);
                drinkAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet5.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });
            }
        });
        fastFoodVM.getFastData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet6=listFoodResponse;
                fastFoodAdapter=new BestFoodHomeRecyclerAdapter(dataSet6,HomeFragment.this::onItemClick,getContext());
                rcvFastFood.setAdapter(fastFoodAdapter);
                fastFoodAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet6.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });
            }
        });
        healthyVM.getHealthyData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet7=listFoodResponse;
                healthyAdapter=new BestFoodHomeRecyclerAdapter(dataSet7,HomeFragment.this::onItemClick,getContext());
                rcvHealthy.setAdapter(healthyAdapter);
                healthyAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet7.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });
            }
        });
        vegetarianVM.getVegetarianData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet8=listFoodResponse;
                vegetarianAdapter=new BestFoodHomeRecyclerAdapter(dataSet8,HomeFragment.this::onItemClick,getContext());
                rcvVegetarian.setAdapter(vegetarianAdapter);
                vegetarianAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet8.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });
            }
        });
        dessertVM.getDessertData().observe(this, new Observer<ListFoodResponse>() {
            @Override
            public void onChanged(ListFoodResponse listFoodResponse) {
                dataSet9=listFoodResponse;
                dessertAdapter=new BestFoodHomeRecyclerAdapter(dataSet9,HomeFragment.this::onItemClick,getContext());
                rcvDessert.setAdapter(dessertAdapter);
                dessertAdapter.setOnItemClickListener(new TopFoodOnRecyclerClick() {
                    @Override
                    public void onItemClick(int position) {
                        String id = dataSet9.getResults().get(position).getId()+ "";
                        Bundle bundle = new Bundle();
                        bundle.putString("id",id);
                        MainActivity.replace((AppCompatActivity) getActivity(),new DetailFragment(),bundle);
                    }
                });
            }
        });
    }
}
