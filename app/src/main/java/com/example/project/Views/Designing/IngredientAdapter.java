package com.example.project.Views.Designing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Model.api.response.IngredientResponse;
import com.example.project.R;

import java.text.DecimalFormat;

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder> {

    private IngredientResponse dataSet;
    private TopFoodOnRecyclerClick listener;
    private Context context;

    public IngredientAdapter(IngredientResponse dataSet, Context context,TopFoodOnRecyclerClick listener) {
        this.dataSet = dataSet;
        this.context = context;
        this.listener=listener;
    }

    @NonNull
    @Override
    public IngredientAdapter.IngredientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ingredient_list_view,parent,false);
        return new IngredientAdapter.IngredientViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IngredientAdapter.IngredientViewHolder holder, int position) {
        Double amount = dataSet.getIngredients().get(position).getAmount();
        DecimalFormat df = new DecimalFormat("#.##");
        holder.tvAmount.setText(df.format(amount)+" "
                               +dataSet.getIngredients().get(position).getUnit());
        holder.tvIngredient.setText(dataSet.getIngredients().get(position).getName());
    }

    public void setOnItemClickListener(TopFoodOnRecyclerClick listener)
    {
        this.listener=listener;
    }

    @Override
    public int getItemCount() {
        return dataSet.getIngredients().size();
    }

    public class IngredientViewHolder extends RecyclerView.ViewHolder {
        TextView tvAmount,tvIngredient;
        public IngredientViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAmount=itemView.findViewById(R.id.amount);
            tvIngredient=itemView.findViewById(R.id.ingredient);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if (position!=RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            });
        }
    }
}
