package com.example.project.Views.SimpleEntity;

public class BestFoodEntity {
    private Integer imageBestFood;
    private String titleBestFood;
    private Integer minuteBestFood;
    private Integer servingBestFood;

    public BestFoodEntity(Integer imageBestFood, String titleBestFood, Integer minuteBestFood, Integer servingBestFood) {
        this.imageBestFood = imageBestFood;
        this.titleBestFood = titleBestFood;
        this.minuteBestFood = minuteBestFood;
        this.servingBestFood = servingBestFood;
    }

    public Integer getImageBestFood() {
        return imageBestFood;
    }

    public void setImageBestFood(Integer imageBestFood) {
        this.imageBestFood = imageBestFood;
    }

    public String getTitleBestFood() {
        return titleBestFood;
    }

    public void setTitleBestFood(String titleBestFood) {
        this.titleBestFood = titleBestFood;
    }

    public Integer getMinuteBestFood() {
        return minuteBestFood;
    }

    public void setMinuteBestFood(Integer minuteBestFood) {
        this.minuteBestFood = minuteBestFood;
    }

    public Integer getServingBestFood() {
        return servingBestFood;
    }

    public void setServingBestFood(Integer servingBestFood) {
        this.servingBestFood = servingBestFood;
    }
}

