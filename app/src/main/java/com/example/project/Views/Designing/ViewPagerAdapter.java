package com.example.project.Views.Designing;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.project.R;
import com.example.project.Views.Fragment.EndlessFragment;
import com.example.project.Views.Fragment.ExploreFragment;
import com.example.project.Views.Fragment.HomeFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private int tabCount;
    private int[] tab_icon = {
            R.drawable.ic_home_black_24dp,
            R.drawable.ic_settings_black_24dp,
            R.drawable.ic_explore_black_24dp,

    };

    public ViewPagerAdapter(@NonNull FragmentManager fm, int tabCount) {
        super(fm, tabCount);
        this.tabCount=tabCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        switch (position){
            case 0:
                fragment = new HomeFragment();break;
            case 1:
                fragment = new EndlessFragment();break;
            case 2:
                fragment = new ExploreFragment();break;
        }
        return fragment;
    }

    public void replaceInViewPager(int position,Fragment fragment){
        Fragment fragment1=null;
        switch (position){
            case 0:
                fragment1 = new HomeFragment();break;
            case 1:
                fragment1 = new EndlessFragment();break;
            case 2:
                fragment1 = new ExploreFragment();break;
        }
    }

//    @Nullable
//    @Override
//    public CharSequence getPageTitle(int position) {
//        CharSequence title = "";
//        switch (position) {
//            case 0:
//                title = "Home";break;
//            case 1:
//                title = "Explore";break;
//            case 2:
//                title = "Setting";break;
//        }
//        return title;
//    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
