package com.example.project.Views.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.project.Model.api.response.IngredientResponse;
import com.example.project.R;
import com.example.project.ViewModel.IngredientViewModel;
import com.example.project.Views.Designing.EndlessAdapter;
import com.example.project.Views.Designing.IngredientAdapter;
import com.example.project.Views.Designing.TopFoodOnRecyclerClick;

public class DetailFragment extends Fragment implements TopFoodOnRecyclerClick {

    private IngredientResponse dataSet;
    private IngredientAdapter ingredientAdapter;
    private RecyclerView rcvingredients;
    private ImageView detailImage;
    private TextView detailTitle,detailMinute,detailServe,detailInstruction;
    private IngredientViewModel ingredientViewModel;
    public Integer position;
    public Integer id1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View viewFragment = inflater.inflate(R.layout.detail_view,null);

        rcvingredients=viewFragment.findViewById(R.id.ingredient_rcv);
        detailImage=viewFragment.findViewById(R.id.detail_image);
        detailTitle=viewFragment.findViewById(R.id.detail_title);
        detailMinute=viewFragment.findViewById(R.id.detail_minute);
        detailServe=viewFragment.findViewById(R.id.detail_serving);
        detailInstruction=viewFragment.findViewById(R.id.instruction);

        Bundle bundle = getArguments();
        String id = bundle.getString("id");
        id1=Integer.parseInt(id);

        ingredientViewModel= ViewModelProviders.of(this).get(IngredientViewModel.class);
        ingredientViewModel.ingredientInit(id);

        getLiveDataFromViewModel();

        return viewFragment;
    }

    void setUpRCV(){
        ingredientAdapter=new IngredientAdapter(dataSet,getContext(),this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        rcvingredients.setLayoutManager(layoutManager);
        rcvingredients.setAdapter(ingredientAdapter);
    }

    public void getLiveDataFromViewModel(){
        ingredientViewModel.getIngredientData().observe(this, new Observer<IngredientResponse>() {
            @Override
            public void onChanged(IngredientResponse ingredientResponse) {
                dataSet=ingredientResponse;
                Glide.with(getContext()).load(dataSet.getImage()).into(detailImage);
                detailTitle.setText(dataSet.getTitle());
                detailMinute.setText(dataSet.getReadyInMinutes() + " minutes");
                detailServe.setText(dataSet.getServings() + " people");
                detailInstruction.setText("\t\t\t\t\t\t"+dataSet.getInstructions());
                setUpRCV();
            }
        });
    }

    @Override
    public void onItemClick(int position) {

    }


//    public void onWindowFocusChanged(boolean hasFocus) {
//        // get content height
//        int contentHeight = ingredientListView.getChildAt(0).getHeight()*indList.size();
//
//        // set listview height
//        ViewGroup.LayoutParams lp = ingredientListView.getLayoutParams();
//        lp.height = contentHeight;
//        ingredientListView.setLayoutParams(lp);
//    }
}
