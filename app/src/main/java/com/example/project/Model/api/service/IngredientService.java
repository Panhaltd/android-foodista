package com.example.project.Model.api.service;

import com.example.project.Model.api.response.IngredientResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IngredientService {
    @GET("recipes/{id}/information")
    Call<IngredientResponse> getIngredientByID(@Path("id") int id, @Query("apiKey") String apiKey);

}
