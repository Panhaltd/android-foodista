package com.example.project.Model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultsEntity {
    @Expose
    @SerializedName("image")
    public String image;
    @Expose
    @SerializedName("servings")
    public int servings;
    @Expose
    @SerializedName("readyInMinutes")
    public int readyInMinutes;
    @Expose
    @SerializedName("title")
    public String title;
    @Expose
    @SerializedName("id")
    public int id;

    public ResultsEntity(String image, int servings, int readyInMinutes, String title, int id) {
        this.image = image;
        this.servings = servings;
        this.readyInMinutes = readyInMinutes;
        this.title = title;
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public int getReadyInMinutes() {
        return readyInMinutes;
    }

    public void setReadyInMinutes(int readyInMinutes) {
        this.readyInMinutes = readyInMinutes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
