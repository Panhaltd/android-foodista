package com.example.project.Model.api.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.project.Model.api.config.ServiceGenerator;
import com.example.project.Model.api.response.ListFoodResponse;
import com.example.project.Model.api.service.FoodRecipeService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private FoodRecipeService foodRecipeService;

    public ArticleRepository(){
        foodRecipeService= ServiceGenerator.createService(FoodRecipeService.class);
    }

//    //https://spoonacular.com/recipeImages/

    public MutableLiveData<ListFoodResponse> getBreakfastByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataBreakFast = new MutableLiveData<>();
        Call<ListFoodResponse> breakFastcall= foodRecipeService.getByCategory("breakfast",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        breakFastcall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataBreakFast.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataBreakFast;
    }
    public MutableLiveData<ListFoodResponse> getLunchByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataLunch = new MutableLiveData<>();
        Call<ListFoodResponse> lunchcall= foodRecipeService.getByCategory("lunch",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        lunchcall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataLunch.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataLunch;
    }
    public MutableLiveData<ListFoodResponse> getDinnerByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataDinner = new MutableLiveData<>();
        Call<ListFoodResponse> dinnercall= foodRecipeService.getByCategory("soup",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        dinnercall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataDinner.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataDinner;
    }
    public MutableLiveData<ListFoodResponse> getDrinkByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataDrink = new MutableLiveData<>();
        Call<ListFoodResponse> drinkcall= foodRecipeService.getByCategory("drink",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        drinkcall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataDrink.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataDrink;
    }
    public MutableLiveData<ListFoodResponse> getFastFoodByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataFastFood = new MutableLiveData<>();
        Call<ListFoodResponse> fastFoodcall= foodRecipeService.getByCategory("burger",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        fastFoodcall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataFastFood.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataFastFood;
    }
    public MutableLiveData<ListFoodResponse> getHealthyByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataHealthy = new MutableLiveData<>();
        Call<ListFoodResponse> healthycall= foodRecipeService.getByCategory("healthy",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        healthycall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataHealthy.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataHealthy;
    }
    public MutableLiveData<ListFoodResponse> getVegetarianByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataVegetarian = new MutableLiveData<>();
        Call<ListFoodResponse> vegetariancall= foodRecipeService.getByCategory("vegetarian",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        vegetariancall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataVegetarian.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataVegetarian;
    }
    public MutableLiveData<ListFoodResponse> getDessertByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataDessert = new MutableLiveData<>();
        Call<ListFoodResponse> dessertCall= foodRecipeService.getByCategory("dessert",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        dessertCall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataDessert.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataDessert;
    }
    public MutableLiveData<ListFoodResponse> getBestByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveDataBestFood = new MutableLiveData<>();
        Call<ListFoodResponse> bestFoodcall= foodRecipeService.getByCategory("best",listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        bestFoodcall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataBestFood.setValue(response.body());
                }
            }
            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataBestFood;
    }

    public MutableLiveData<ListFoodResponse> getCategoryByListFoodResponse(ListFoodResponse listFoodResponse,String category){
        final MutableLiveData<ListFoodResponse> liveDataBestFood = new MutableLiveData<>();
        Call<ListFoodResponse> bestFoodcall= foodRecipeService.getByCategory(category,listFoodResponse.getNumber(),"477634a81d034712822687b7d14c5963");
        bestFoodcall.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveDataBestFood.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {

            }
        });
        return liveDataBestFood;
    }

    public MutableLiveData<ListFoodResponse> getListArticleByListFoodResponse(ListFoodResponse listFoodResponse){
        final MutableLiveData<ListFoodResponse> liveData = new MutableLiveData<>();
        Call<ListFoodResponse> call= foodRecipeService.getAll(listFoodResponse.getNumber(),listFoodResponse.getOffset(),"477634a81d034712822687b7d14c5963");
        call.enqueue(new Callback<ListFoodResponse>() {
            @Override
            public void onResponse(Call<ListFoodResponse> call, Response<ListFoodResponse> response) {
                if (response.isSuccessful()){
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListFoodResponse> call, Throwable t) {
                Log.e("error","OnError"+t.getMessage());
            }
        });
        return liveData;
    }

}
