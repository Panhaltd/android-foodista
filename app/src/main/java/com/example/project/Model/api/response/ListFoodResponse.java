package com.example.project.Model.api.response;

import com.example.project.Model.entity.ResultsEntity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListFoodResponse {

    @Expose
    @SerializedName("number")
    public int number;
    @Expose
    @SerializedName("offset")
    public int offset;
    @Expose
    @SerializedName("baseUri")
    public String baseUri;
    @Expose
    @SerializedName("totalResults")
    public int totalResults;

    public int getTotalResults() {
        return totalResults;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    @Expose
    @SerializedName("results")
    public List<ResultsEntity> results;

    public ListFoodResponse() {
        this.number=60;
        this.offset=1;
    }

    public ListFoodResponse(int number,int offset) {
        this.number = number;
        this.offset=offset;
    }

    public ListFoodResponse(int number, String baseUri, List<ResultsEntity> results) {
        this.number = number;
        this.baseUri = baseUri;
        this.results = results;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public void setBaseUri(String baseUri) {
        this.baseUri = baseUri;
    }

    public List<ResultsEntity> getResults() {
        return results;
    }

    public void setResults(List<ResultsEntity> results) {
        this.results = results;
    }
}
