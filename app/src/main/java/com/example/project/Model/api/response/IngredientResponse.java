package com.example.project.Model.api.response;

import com.example.project.Model.entity.IngredientsEntity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IngredientResponse {

    @Expose
    @SerializedName("image")
    private String image;
    @Expose
    @SerializedName("servings")
    private int servings;
    @Expose
    @SerializedName("readyInMinutes")
    private int readyInMinutes;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("instructions")
    private String instructions;
    @Expose
    @SerializedName("pairingText")
    private String pairingText;
    @Expose
    @SerializedName("extendedIngredients")
    private List<IngredientsEntity> ingredients;
    private String baseUrl;

    public String getBaseUrl() {
        return "https://spoonacular.com/recipeImages/";
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public int getReadyInMinutes() {
        return readyInMinutes;
    }

    public void setReadyInMinutes(int readyInMinutes) {
        this.readyInMinutes = readyInMinutes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getPairingText() {
        return pairingText;
    }

    public void setPairingText(String pairingText) {
        this.pairingText = pairingText;
    }

    public List<IngredientsEntity> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientsEntity> ingredients) {
        this.ingredients = ingredients;
    }
}
