package com.example.project.Model.api.service;

import com.example.project.Model.api.response.IngredientResponse;
import com.example.project.Model.api.response.ListFoodResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface FoodRecipeService {

    @GET("recipes/search")
    Call<ListFoodResponse> getAll(@Query("number") int number,@Query("offset") int offset, @Query("apiKey") String apiKey);

    @GET("recipes/search")
    Call<ListFoodResponse> getByCategory(@Query("query") String type
                                        ,@Query("number") int number
                                        ,@Query("apiKey") String apiKey);
    @GET("recipes/{id}/information")
    Call<IngredientResponse> getIngredientByID(@Part("id") String id,@Query("apiKey") String apiKey);
}
