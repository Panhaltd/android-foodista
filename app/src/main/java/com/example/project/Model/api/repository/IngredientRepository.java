package com.example.project.Model.api.repository;

import androidx.lifecycle.MutableLiveData;

import com.example.project.Model.api.config.ServiceGenerator;
import com.example.project.Model.api.response.IngredientResponse;
import com.example.project.Model.api.service.IngredientService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IngredientRepository {

    private IngredientService ingredientService;

    public IngredientRepository(){
        ingredientService= ServiceGenerator.createService(IngredientService.class);
    }
    public MutableLiveData<IngredientResponse> getIngredientByResponse(IngredientResponse ingredientResponse,String id_param){
        final MutableLiveData<IngredientResponse> liveDataIngredient = new MutableLiveData<>();

        Integer id = Integer.parseInt(id_param);

        Call<IngredientResponse> ingredientCall= ingredientService.getIngredientByID(id,"477634a81d034712822687b7d14c5963");
        ingredientCall.enqueue(new Callback<IngredientResponse>() {
            @Override
            public void onResponse(Call<IngredientResponse> call, Response<IngredientResponse> response) {
                liveDataIngredient.setValue(response.body());
            }

            @Override
            public void onFailure(Call<IngredientResponse> call, Throwable t) {

            }
        });
        return liveDataIngredient;
    }
}
