<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string gender="unknown" name="com_facebook_device_auth_instructions">&lt;b>facebook.com/device&lt;/b>ని సందర్శించి ఎగువన చూపిన కోడ్‌ను నమోదు చేయండి.</string>
    <string gender="unknown" name="com_facebook_image_download_unknown_error">ఒక చిత్రాన్ని డౌన్‌లోడ్ చేయడంలో అనుకోని లోపం సంభవించింది.</string>
    <string gender="unknown" name="com_facebook_internet_permission_error_message">WebView లాగిన్ కోసం ఇంటర్నెట్ అనుమతి అవసరమవుతుంది</string>
    <string gender="unknown" name="com_facebook_internet_permission_error_title">AndroidManifest లోపం</string>
    <string gender="unknown" name="com_facebook_like_button_liked">ఇష్టపడ్డారు</string>
    <string gender="unknown" name="com_facebook_like_button_not_liked">ఇష్టం</string>
    <string gender="unknown" name="com_facebook_loading">లోడ్ చేస్తోంది…</string>
    <string gender="unknown" name="com_facebook_loginview_cancel_action">రద్దు చేయి</string>
    <string gender="unknown" name="com_facebook_loginview_log_in_button">లాగిన్</string>
    <string gender="unknown" name="com_facebook_loginview_log_in_button_continue">Facebookతో కొనసాగించు</string>
    <string gender="unknown" name="com_facebook_loginview_log_in_button_long">Facebookతో లాగిన్ చేయండి</string>
    <string gender="unknown" name="com_facebook_loginview_log_out_action">లాగ్ అవుట్ చేయండి</string>
    <string gender="unknown" name="com_facebook_loginview_log_out_button">లాగ్ అవుట్ చేయండి</string>
    <string gender="unknown" name="com_facebook_loginview_logged_in_as">వీరి వలె లాగిన్ చేసారు: %1$s</string>
    <string gender="unknown" name="com_facebook_loginview_logged_in_using_facebook">Facebookని ఉపయోగించి లాగిన్ చేసారు</strin